import {ITrackInfo} from '../src/app/model/trackinfo.interface';

export const trackInfo: ITrackInfo[] = [
  {
    data: new Date(2018, 11, 15),
    steps: 7500,
    distance: 7,
    duration: 19,
    burn: 4567
  },
  {
    data: new Date(2018, 11, 15),
    steps: 8000,
    distance: 7.5,
    duration: 2,
    burn: 5000
  },
  {
    data: new Date(),
    steps: 8500,
    distance: 8.5,
    duration: 2.4,
    burn: 6000
  }
];
