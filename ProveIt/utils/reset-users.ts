import {database, initializeApp} from 'firebase';
import {environment} from '../src/environments/environment';

initializeApp(environment.firebaseConfig);

const usersRef = database().ref('users');

async function reset() {
    await usersRef.set(null);

    // tslint:disable-next-line
    console.log('🔥 users reset 🔥');

    process.exit();
}

reset();
