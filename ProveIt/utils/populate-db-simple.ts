import {database, initializeApp} from 'firebase';
import {environment} from '../src/environments/environment';
import {trackInfo} from './trackInfo-db';


initializeApp(environment.firebaseConfig);

async function populate(name: string, data: any[]): Promise<any> {
    const ref = database().ref(name);

    await ref.set(null);

    const promises = data.map(i => ref.push(i));

    return Promise.all(promises);
}

async function populateAll() {
    await populate('trackInfo', trackInfo);

    // tslint:disable-next-line
    console.log('🔥 db populated 🔥');
    process.exit();
}

populateAll();
