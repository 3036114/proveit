import {Routes} from '@angular/router';
import {IntroComponent} from './modules/welcomePage/intro/components/intro/intro.component';
import {NotFoundPageComponent} from './modules/notFoundPage/components/not-found-page/not-found-page.component';
import {LoginComponent} from './modules/auth/login/components/login/login.component';
import {MainPanelComponent} from './modules/trackerPage/components/main-panel/main-panel.component';
import {StatisticsComponent} from './modules/statisticsPage/components/statistics/statistics.component';

export const appRoutes: Routes = [
  {
    path: 'main-panel',
    component: MainPanelComponent
  },
  {
    path: '',
    component: IntroComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'statistics',
    component: StatisticsComponent
  },
  {
    path: '**',
    component: NotFoundPageComponent
  },
];
