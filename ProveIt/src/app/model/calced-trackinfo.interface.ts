import {ITrackInfo} from './trackinfo.interface';

export interface ICalcedTrackInfo {
  trackInfo: ITrackInfo[];
  totalSteps: number;
}
