export interface ITrackInfo {
  id?: string;
  data: Date;
  steps: number;
  distance?: number;
  duration: number;
  burn?: number;
}
