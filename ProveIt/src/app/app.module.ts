import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {SidebarModule} from './modules/sidebar/components/sidebar/sidebar.module';
import {RouterModule} from '@angular/router';
import {IntroModule} from './modules/welcomePage/intro/components/intro/intro.module';
import {appRoutes} from './app.routing';
import {NotFoundPageModule} from './modules/notFoundPage/components/not-found-page/not-found-page.module';
import {LoginModule} from './modules/auth/login/components/login/login.module';
import {MainPanelModule} from './modules/trackerPage/components/main-panel/main-panel.module';
import {HttpClientModule} from '@angular/common/http';
import {StatisticsModule} from './modules/statisticsPage/components/statistics/statistics.module';

import localeRu from '@angular/common/locales/ru';
import {registerLocaleData} from '@angular/common';

registerLocaleData(localeRu, 'ru');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MainPanelModule,
    SidebarModule,
    IntroModule,
    RouterModule.forRoot(appRoutes),
    NotFoundPageModule,
    LoginModule,
    HttpClientModule,
    StatisticsModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'ru' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
