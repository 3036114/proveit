import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatisticsComponent } from './statistics.component';
import {MyBarChartModule} from '../my-bar-chart/my-bar-chart.module';
import {PanelService} from '../../../trackerPage/components/panel/panel.service';
import {PanelHttpService} from '../../../trackerPage/components/panel/panel-http.service';

@NgModule({
  declarations: [StatisticsComponent],
  imports: [
    CommonModule,
    MyBarChartModule
  ],
  exports: [StatisticsComponent],
  providers: [
    PanelService,
    PanelHttpService
  ]
})
export class StatisticsModule { }
