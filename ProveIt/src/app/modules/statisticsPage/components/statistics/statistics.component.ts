import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {PanelHttpService} from '../../../trackerPage/components/panel/panel-http.service';
import {ITrackInfo} from '../../../../model/trackinfo.interface';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  trackerInfo$: Observable<ITrackInfo[]>;

  constructor(private panelHttpService: PanelHttpService) { }

  ngOnInit() {
    this.trackerInfo$ = this.panelHttpService.getData();
  }

}
