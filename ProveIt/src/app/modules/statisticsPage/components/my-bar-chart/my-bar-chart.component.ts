import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ITrackInfo} from '../../../../model/trackinfo.interface';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-my-bar-chart',
  templateUrl: './my-bar-chart.component.html',
  styleUrls: ['./my-bar-chart.component.css']
})
export class MyBarChartComponent implements OnChanges {
  @Input() trackInfo: ITrackInfo[];

  pipe = new DatePipe('ru');

  barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  barChartLabels = [];

  barChartData = [
    {data: [], label: 'Шаги'},
    {data: [], label: 'Км'},
    {data: [], label: 'Ккал'}
  ];

  ngOnChanges() {

    if (Array.isArray(this.trackInfo)) {
      console.log('asdasdasd', this.trackInfo);


      this.trackInfo.forEach(item => {
        this.barChartData[0].data.push(item.steps);
        this.barChartData[1].data.push(item.distance);
        this.barChartData[2].data.push(item.burn);

        this.barChartLabels.push(this.pipe.transform(item.data, 'shortDate'));
      });

    }
  }
}
