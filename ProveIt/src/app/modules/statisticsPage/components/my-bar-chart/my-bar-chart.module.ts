import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyBarChartComponent } from './my-bar-chart.component';
import {ChartsModule} from 'ng2-charts';

@NgModule({
  declarations: [MyBarChartComponent],
  imports: [
    CommonModule,
    ChartsModule
  ],
  exports: [MyBarChartComponent]
})
export class MyBarChartModule { }
