import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ITrackInfo} from '../../../../../model/trackinfo.interface';

@Component({
  selector: 'app-track-preview',
  templateUrl: './track-preview.component.html',
  styleUrls: ['./track-preview.component.css']
})
export class TrackPreviewComponent {
  @Input() trackPreview: ITrackInfo;
  @Output() trackPreviewDelete = new EventEmitter<void>();

  onDeleteClick() {
    this.trackPreviewDelete.emit();
  }
}
