import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ITrackInfo} from '../../../../model/trackinfo.interface';
import {map} from 'rxjs/operators';
import {tap} from 'rxjs/internal/operators/tap';

const BASE_URL = 'https://proveit-2899e.firebaseio.com';

@Injectable()
export class PanelHttpService {

  constructor(private http: HttpClient) {}

  getData(): Observable<ITrackInfo[]> {
    return this.http.get(`${BASE_URL}/trackInfo.json`)
      .pipe(
        map<any, ITrackInfo[]>(data => Object.entries(data).map(([id, value]) => ({...value, id} as ITrackInfo))),
        tap(console.log)
      );
  }

  addTrackInfo(trackInfo: ITrackInfo): Observable<any> {
    return this.http.post(`${BASE_URL}/trackInfo.json`, trackInfo);
  }

  deleteTrackInfo(id: string): Observable<any> {
    return this.http.delete(`${BASE_URL}/trackInfo/${id}.json`);
  }
}
