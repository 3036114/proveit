import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent } from './panel.component';
import { TrackPreviewComponent } from './track-preview/track-preview.component';
import {PipeModule} from '../../../../pipes/pipe.module';
import {AddTrackModule} from '../add-track/add-track.module';
import {PanelService} from './panel.service';
import {PanelHttpService} from './panel-http.service';

@NgModule({
  declarations: [PanelComponent, TrackPreviewComponent],
  imports: [
    CommonModule,
    PipeModule,
    AddTrackModule,
  ],
  exports: [PanelComponent, TrackPreviewComponent],
  providers: [
    PanelService,
    PanelHttpService
  ]
})
export class PanelModule { }
