import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ITrackInfo} from '../../../../model/trackinfo.interface';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent {
  @Input() trackInfo: ITrackInfo[] = [];
  @Output() trackPreviewDelete = new EventEmitter<ITrackInfo>();
  @Output() addTrackInfo = new EventEmitter<ITrackInfo>();

  isAddTrackInfoShow = false;

  constructor() {}

  toggleAdd() {
    this.isAddTrackInfoShow = !this.isAddTrackInfoShow;
  }
}
