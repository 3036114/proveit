import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPanelComponent } from './main-panel.component';
import {PanelModule} from '../panel/panel.module';
import {TopRaitingModule} from '../top-raiting/top-raiting.module';
import {PanelService} from '../panel/panel.service';
import {PanelHttpService} from '../panel/panel-http.service';

@NgModule({
  declarations: [MainPanelComponent],
  imports: [
    CommonModule,
    PanelModule,
    TopRaitingModule
  ],
  exports: [MainPanelComponent],
  providers: [
    PanelService,
    PanelHttpService
  ]
})
export class MainPanelModule { }
