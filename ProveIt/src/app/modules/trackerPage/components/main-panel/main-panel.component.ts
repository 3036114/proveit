import { Component, OnInit } from '@angular/core';
import {ITrackInfo} from '../../../../model/trackinfo.interface';
import {PanelService} from '../panel/panel.service';
import {PanelHttpService} from '../panel/panel-http.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-main-panel',
  templateUrl: './main-panel.component.html',
  styleUrls: ['./main-panel.component.css']
})
export class MainPanelComponent implements OnInit {

  trackerInfo$: Observable<ITrackInfo[]>;

  isAddTrackInfoShow = !true;

  constructor(private panelService: PanelService, private panelHttpService: PanelHttpService) { }

  ngOnInit() {
    this.trackerInfo$ = this.panelHttpService.getData();
  }

  onDelete(track: ITrackInfo) {
      // debugger;
      this.panelHttpService.deleteTrackInfo(track.id)
        .subscribe(() => this.trackerInfo$ = this.panelHttpService.getData());
  }

  toggleAdd() {
    this.isAddTrackInfoShow = !this.isAddTrackInfoShow;
  }

  onAddTrack(data) {
    this.panelHttpService.addTrackInfo(data)
      .subscribe(() => {
        this.toggleAdd();
        this.trackerInfo$ = this.panelHttpService.getData();
      });
  }
}
