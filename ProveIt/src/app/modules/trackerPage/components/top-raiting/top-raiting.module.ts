import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopRaitingComponent } from './top-raiting.component';
import {PipeModule} from '../../../../pipes/pipe.module';
import {PanelService} from '../panel/panel.service';
import {PanelModule} from '../panel/panel.module';

@NgModule({
  declarations: [TopRaitingComponent],
  imports: [
    CommonModule,
    PipeModule,
    PanelModule
  ],
  exports: [TopRaitingComponent],
  providers: [
    PanelService,
  ]
})
export class TopRaitingModule { }
