import {Component, Input, OnInit} from '@angular/core';
import {ITrackInfo} from '../../../../model/trackinfo.interface';

@Component({
  selector: 'app-top-raiting',
  templateUrl: './top-raiting.component.html',
  styleUrls: ['./top-raiting.component.css']
})
export class TopRaitingComponent implements OnInit {
  @Input() trackInfo: ITrackInfo[];

  constructor() { }

  ngOnInit() {
    console.log('trackInfo', this.trackInfo);
  }

  get getMaxStep(): number {
    return !!this.trackInfo ? this.trackInfo
      .sort((a, b) => a.steps - b.steps)
      .reverse()[0]
      .steps
      : 0;
  }

    get getMaxDistance(): number {
        return !!this.trackInfo ? this.trackInfo
                .sort((a, b) => a.distance - b.distance)
                .reverse()[0]
                .distance
            : 0;
    }

    get getMaxBurn(): number {
        return !!this.trackInfo ? this.trackInfo
                .sort((a, b) => a.burn - b.burn)
                .reverse()[0]
                .burn
            : 0;
    }

    get getMinSteps(): number {
        return !!this.trackInfo ? this.trackInfo
                .sort((a, b) => a.steps - b.steps)[0]
                .steps
            : 0;
    }

    get getMinDistance(): number {
        return !!this.trackInfo ? this.trackInfo
                .sort((a, b) => a.distance - b.distance)[0]
                .distance
            : 0;
    }

    get getMinBurn(): number {
        return !!this.trackInfo ? this.trackInfo
                .sort((a, b) => a.burn - b.burn)[0]
                .burn
            : 0;
    }

    get getTotalSteps(): number {
        return this.trackInfo.reduce((totalSteps, {steps}) => totalSteps + steps, 0);
    }

    get getTotalDistance(): number {
        return this.trackInfo.reduce((totalDistance, {distance}) => totalDistance + distance, 0);
    }

    get getTotalBurn(): number {
        return this.trackInfo.reduce((totalBurn, {burn}) => totalBurn + burn, 0);
    }

    get getAverageSteps(): number {
        return this.getTotalSteps / this.trackInfo.length;
    }

    get getAverageDistance(): number {
        return this.getTotalDistance / this.trackInfo.length;
    }

    get getAverageBurn(): number {
        return this.getTotalBurn / this.trackInfo.length;
    }
}
