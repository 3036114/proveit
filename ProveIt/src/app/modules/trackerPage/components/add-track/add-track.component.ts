import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {ITrackInfo} from '../../../../model/trackinfo.interface';

const onlyNumbers = /^\d+$/

@Component({
  selector: 'app-add-track',
  templateUrl: './add-track.component.html',
  styleUrls: ['./add-track.component.css']
})
export class AddTrackComponent implements OnInit {
  @Output() addTrackInfo = new EventEmitter<ITrackInfo>();
  form: FormGroup;

  constructor() {}

  getErrors(control: AbstractControl): string {
    const {errors} = control;
    if (!errors) {
      return '';
    }
    if (errors['required']) {
      return 'Поле обязательно для заполнения';
    }
    if (errors['minlength']) {
      return 'Минимальная количество пройденных шагов 100';
    }
    if (errors['pattern']) {
      return 'Введите число';
    }
  }

  onSubmit() {
    const data = new Date(this.form.value.data);

    if (this.form.invalid) {
      return;
    }

    const steps = parseInt(this.form.value.steps, 10);

    if (isNaN(steps) || steps <= 0) {
      return;
    }
    const duration = this.form.value.duration;

    const distanceK = ((170 / 4) + 37) / 100;
    const distance = (steps * distanceK) / 1000;
    if (distance <= 0) {
      return;
    }
    const burn = distance * 80 * 0.5;
    const trackInfo: ITrackInfo = {
      data,
      steps,
      distance,
      duration,
      burn
    };
    console.log('---trackInfo', trackInfo);
    this.addTrackInfo.emit(trackInfo);
  }

  ngOnInit() {
    this.form = new FormGroup({
      data: new FormControl( '', [Validators.required]),
      steps: new FormControl('', [Validators.required, Validators.minLength(3), Validators.pattern(onlyNumbers)]),
      distance: new FormControl(''),
      duration: new FormControl('00:30', ),
      burn: new FormControl(''),
    });
  }
}
