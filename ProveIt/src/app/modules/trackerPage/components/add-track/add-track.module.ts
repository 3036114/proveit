import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { AddTrackComponent } from './add-track.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [AddTrackComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [AddTrackComponent]
})
export class AddTrackModule { }
