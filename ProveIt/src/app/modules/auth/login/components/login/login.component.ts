import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  getErrors(control: AbstractControl): string {
    const {errors} = control;
    if (!errors) {
      return '';
    }
    if (errors['required']) {
      return 'Это поле обязательно для заполнения';
    }
    if (errors['email']) {
      return 'Укажите электронную почту в формате name@someting.any';
    }
    if (errors['minlength']) {
      return 'Пароль дожен содержать не менее 6 символов';
    }
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['3036114@gmail.com', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }
}
