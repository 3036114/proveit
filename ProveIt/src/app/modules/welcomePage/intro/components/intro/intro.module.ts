import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntroComponent } from './intro.component';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [IntroComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [IntroComponent]
})
export class IntroModule { }
