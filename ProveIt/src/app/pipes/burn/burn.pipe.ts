import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'burn'
})
export class BurnPipe implements PipeTransform {

  transform(value: number): string {
    return `🔥 ${value} Ккал`;
  }

}
