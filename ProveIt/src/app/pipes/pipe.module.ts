import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DistancePipe} from './distance-pipe/distance.pipe';
import { StepPipe } from './step/step.pipe';
import { DurationPipe } from './duration/duration.pipe';
import { BurnPipe } from './burn/burn.pipe';
import { MathfloorPipe } from './mathfloor/mathfloor.pipe';

@NgModule({
  declarations: [DistancePipe, StepPipe, DurationPipe, BurnPipe, MathfloorPipe],
  imports: [
    CommonModule
  ],
  exports: [DistancePipe, StepPipe, DurationPipe, BurnPipe, MathfloorPipe]
})
export class PipeModule { }
