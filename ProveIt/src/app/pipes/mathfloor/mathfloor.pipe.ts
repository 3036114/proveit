import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mathfloor'
})
export class MathfloorPipe implements PipeTransform {

  transform(value: number): number {
    return (Math.floor(value * 100) / 100);
  }
}
