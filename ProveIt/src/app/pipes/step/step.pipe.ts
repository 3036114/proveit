import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'step'
})
export class StepPipe implements PipeTransform {

  transform(value: number): string {
    return `👣 ${value} шагов`;
  }

}
